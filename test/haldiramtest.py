import time
import random
from loginPage import LoginPage
from selenium.common.exceptions import NoSuchElementException


class Haldiramtest(LoginPage):
    def __init__(self, configfile, should_login=False):
        LoginPage.__init__(self, configfile)
        self.should_login = should_login

    def run_test(self):
        if self.should_login:
             super(Haldiramtest, self).run_test()
        self.haldiramtesting()

    def haldiramtesting(self):

        url = self.parser.get('store','url')
        response_time = self.get(url)
        self.log(response_time)
        self.driver.maximize_window()
        time.sleep(5)
        try:
            close_popup = self.driver.find_element_by_css_selector('button[type*="button"][data-dismiss ="modal"]')
            close_popup.click()
            self.log( "pop-up closed")
            time.sleep(10)
        except NoSuchElementException:
            self.log("no pop-up")
            pass
        products = None
        found = False
        while not found:
            self.log("Trying to find a product...")
            try:
                menu_list = self.driver.find_elements_by_css_selector('ul[class*="nav nav-wide topmenu navbar-nav"]')
                menu = menu_list.pop(0)
                category_list = menu.find_elements_by_css_selector('li[class*="level0"]')
                container = []
                for eachCategory in category_list:
                    container.append(eachCategory.find_element_by_css_selector('a[class="level-top"]').get_attribute('href'))
                url_categories = "#"
                while "#" in url_categories:
                    url_categories = random.choice(container)

                response_time = self.get(url_categories)
                self.log(response_time)
                self.log("Got page " + url_categories)
                product_list = self.driver.find_elements_by_css_selector('a[href*="http"][class*="product-image"]')
                products = []
                for eachP in product_list:
                    products.append(eachP)
                if len(products) == 0:
                    raise NoSuchElementException
                else:
                    found = True
                    self.log("Product found")
            except NoSuchElementException:
                self.log("Product not found. Trying next category...")
                pass

        finalproductslist = []
        for eachn in products:
            finalproductslist.append(eachn.get_attribute('href'))
        time.sleep(5)
        product_url_found = False
        while product_url_found is False:
            product_url = random.choice(finalproductslist)
            response_time = self.get(product_url)
            self.log(response_time)
            self.log("Product url is.. " + product_url)
            self.log(response_time)
            self.log("Product url is.. " + product_url)
            time.sleep(5)
            self.log("add to cart page")
            try:
                pincodes_list = self.driver.find_elements_by_css_selector('input[id="pincode"][placeholder="Enter Pincode"]')
                if not pincodes_list  :
                    self.log("product out of stock" + product_url)
                    product_url_found = False

                else:
                    pincode = pincodes_list.pop(0)
                    pincode.send_keys("400001")
                    product_url_found = True
                    check_availability = self.driver.find_element_by_css_selector('button[onclick="deliverycheck()"][type="button"]')
                    try:
                        check_availability.submit()
                    except NoSuchElementException:
                        check_availability.click()
                    try:
                        configuration_container = self.driver.find_elements_by_css_selector('div[class="amconf-images-container"]')
                        for container in configuration_container:
                            configurations = container.find_elements_by_css_selector('div[id*="amconf"]')
                        configlist = []
                        for config_el in configurations:
                            configlist.append(config_el)

                        configurations = None

                        while configurations is None:
                            configurations = random.choice(configlist)
                            configurations.click()
                            self.log("we have a configurable product")
                    except Exception:
                        pass
                        self.log("we have a non-configurable product ")
                        time.sleep(5)
                    addtocart= self.driver.find_element_by_css_selector('button[onclick*="productAddToCart"]')
                    addtocart.click()
                    time.sleep(5)
            except NoSuchElementException:
                self.log ("pincode not found")
                pass
            try:
                configurations_container = self.driver.find_elements_by_css_selector('div[class="amconf-images-container"]')
                for container in configurations_container:
                    configurations = container.find_elements_by_css_selector('div[id*="amconf"]')
                configlist = []
                for config_el in configurations:
                    configlist.append(config_el)

                configurations = None

                while configurations is None:
                    configurations = random.choice(configlist)
                    configurations.click()

                self.log("we have a configurable product")
            except Exception:
                pass
                self.log("we have a non-configurable product ")
            time.sleep(5)
            try:
                proceed = self.driver.find_elements_by_css_selector('button[title*="Proceed to Checkout"]')
                proceed_to_checkout= proceed.__getitem__(0)
                self.driver.implicitly_wait(5)
                proceed_to_checkout.click()
            except NoSuchElementException:
                pass
            try:
                #locators
                _field_firstname_ = 'input[Placeholder*="First Name"][title="First Name"]'
                _field_lastname_ = 'input[Placeholder*="Last Name"][title="Last Name"]'
                _field_phone_number = 'input[type*="number"][placeholder*="10 digit number"]'
                _field_email_ = 'input[title*="Email Address"][placeholder*="Email Address"]'

                _field_address_ = 'input[type*="text"][placeholder ="Flat/House No. Colony/Street No."]'
                _keys_email = 'testcase@shoptimize.in'
                _keys_firstname = 'test'
                _keys_lastname = 'case'
                _keys_street = '1/1 aundh/street'
                _keys_phonenumber = '1234567890'
                time.sleep(5)
                self.log('At checkout page')

                script = "window.scrollBy(0, 20);"

                self.driver.execute_script(script)
                firstname = self.driver.find_elements_by_css_selector(_field_firstname_).pop(0)
                firstname.send_keys(_keys_firstname)

                lastname = self.driver.find_elements_by_css_selector(_field_lastname_).pop(0)
                lastname.send_keys(_keys_lastname)

                phone_number = self.driver.find_elements_by_css_selector(_field_phone_number).pop(0)
                phone_number.send_keys(_keys_phonenumber)

                email = self.driver.find_element_by_css_selector(_field_email_)
                email.send_keys(_keys_email)
                time.sleep(2)
                self.driver.execute_script(script)
                street_address = self.driver.find_element_by_css_selector(_field_address_)
                street_address.send_keys(_keys_street)
                time.sleep(5)
            except Exception:
                self.log("could not complete checkout")
                pass

