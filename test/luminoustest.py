import random
import time
from loginPage import LoginPage
from selenium.common.exceptions import NoSuchElementException


class Luminoustest(LoginPage):

    def __init__(self, configfile, should_login=False):
         LoginPage.__init__(self, configfile)
         self.should_login = should_login

    def run_test(self):
        if self.should_login:
             super(Luminoustest, self).run_test()
        self.luminous_productlisting_test()
        self.luminous_addtocart_test()
        self.luminous_checkout_test()

    def luminous_productlisting_test(self):
        url = self.parser.get('store','url')
        response_time = self.get(url)
        self.log(response_time)
        self.driver.maximize_window()
        time.sleep(5)
        self.log("Started luminous test")
        try:
            close_popup = self.driver.find_element_by_css_selector('button[type*="button"][data-dismiss ="modal"]')
            close_popup.click()
            self.log("pop-up closed")
            time.sleep(5)
        except Exception:
            self.log("no pop-up")
            pass


        products = None
        found = False
        while not found:
            self.log("Trying to find a product")
            try:
                menu_list = self.driver.find_elements_by_css_selector('ul[class*="nav nav-wide topmenu navbar-nav"]')
                menu = menu_list.pop(0)
                category_list = menu.find_elements_by_css_selector('li[class*="level0"]')
                container = []
                for eachCategory in category_list:
                    container.append(eachCategory.find_element_by_css_selector('a[class="level-top"]').get_attribute('href'))
                url_categories = "#"

                while "#" in url_categories:
                    url_categories = random.choice(container)

                self.get(url_categories)
                self.log("Got page " + url_categories)
                product_list = self.driver.find_elements_by_css_selector('a[href*="http"][class*="product-image"]')
                products = []
                for eachP in product_list:
                    products.append(eachP)
                if products == []:
                    raise NoSuchElementException
                else:
                    found = True
                    self.log("Category found")
            except Exception:
                self.log("Category not found. Trying next category...")
                pass

        finalproductslist = []
        for each_product in products:
            finalproductslist.append(each_product.get_attribute('href'))

        final_prod_url = False
        while not final_prod_url:
            product_url = random.choice(finalproductslist)
            self.driver.get(product_url)
            time.sleep(5)
            self.log("Product found at " + product_url)
            self.driver.implicitly_wait(5)
            try:
                notify_me_link = self.driver.find_element_by_css_selector('a[class="notify-me-form-link"]')
                if notify_me_link:
                    self.log("Product out of stock")
            except NoSuchElementException:
                final_prod_url = True





    def luminous_addtocart_test(self):

        self.check_click_configurations()

        try:
            pincode_verification = self.driver.find_element_by_css_selector('div[class="pincode-wrapper"] input[id="pincode"]')
            pincode_verification.send_keys("411007")
            pincode_check_button = self.driver.find_element_by_css_selector('div[class="pincode-wrapper"] button')
            pincode_check_button.click()
            self.log("pincode verified")
            time.sleep(8)
        except Exception:
            self.log("Not deliverable to the the Pincode")
            self.run_test()

        self.check_click_configurations()

        try:
            add_to_cart = self.driver.find_element_by_css_selector('button[id*="product-addtocart-button"]')
            add_to_cart.click()
            self.log("Add to cart button clicked")
            time.sleep(5)
        except Exception as e:
            self.log(e)

        try:
            proceed = self.driver.find_elements_by_css_selector('button[title*="Proceed to Checkout"]')
            self.log("proceed to checkout")
            time.sleep(5)
            proceed_to_checkout= proceed.__getitem__(0)
            proceed_to_checkout.click()
            time.sleep(5)
        except NoSuchElementException as e:
            self.log(e)


    def luminous_checkout_test(self):

        #locators
        _field_firstname_ = 'input[Placeholder*="First Name"][title="First Name"]'
        _field_lastname_ = 'input[Placeholder*="Last Name"][title="Last Name"]'
        _field_phone_number = 'input[type*="number"][placeholder*="10 digit number"]'
        _field_email_ = 'input[title*="Email Address"][placeholder*="Email Address"]'

        _field_address_ = 'input[type*="text"][placeholder ="Flat/House No. Colony/Street No."]'
        _keys_email = 'testcase@shoptimize.in'
        _keys_firstname = 'test'
        _keys_lastname = 'case'
        _keys_street = '1/1 aundh/street'
        _keys_phonenumber = '1234567890'
        time.sleep(2)
        self.log('At checkout page')
        script = "window.scrollBy(0, 20);"
        self.driver.execute_script(script)
        firstname = self.driver.find_elements_by_css_selector(_field_firstname_).pop(0)
        firstname.send_keys(_keys_firstname)

        lastname = self.driver.find_elements_by_css_selector(_field_lastname_).pop(0)
        lastname.send_keys(_keys_lastname)

        phone_number = self.driver.find_elements_by_css_selector(_field_phone_number).pop(0)
        phone_number.send_keys(_keys_phonenumber)

        email = self.driver.find_element_by_css_selector(_field_email_)
        email.send_keys(_keys_email)
        time.sleep(2)
        self.driver.execute_script(script)
        street_address = self.driver.find_element_by_css_selector(_field_address_)
        street_address.send_keys(_keys_street)
        time.sleep(5)
        self.log("Finished checkout")


    def check_click_configurations(self):
        try:
            configuration_container = self.driver.find_elements_by_css_selector('div[class="amconf-images-container"]')
            for container in configuration_container:
                configurations = container.find_elements_by_css_selector('div[id*="amconf"]')
            configlist = []
            for config_el in configurations:
                configlist.append(config_el)
            configurations = None

            while configurations is None:
                configurations = random.choice(configlist)
                configurations.click()
                self.log("configurations clicked")
                time.sleep(2)
            self.log("we have a configurable product")

        except Exception:
            self.log("we have a non-configurable product ")
            time.sleep(5)
            pass