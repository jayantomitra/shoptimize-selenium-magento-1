from selenium.common.exceptions import NoSuchElementException
from loginPage import LoginPage
from baseclass import BaseClass
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.webdriver.support.ui import Select
import random
import time

class Lotustest(BaseClass):
    def __init__(self, configfile):
        BaseClass.__init__(self, configfile)

    def run_test(self):
        super(Lotustest, self).run_test()
        self.lotus_testing_productlisting()
        self.lotus_testing_addtocart()
        self.lotus_testing_checkout()



    def lotus_testing_productlisting(self):
        _field_close_pop_up_='button[type*="button"][data-dismiss ="modal"]'
        _field_menu_list_='ul[class*="nav nav-wide topmenu navbar-nav"]'
        _field_category_list_ = 'li[class*="level0"]'
        _field_container_='a[class="level-top"]'
        _field_product_list_='a[href*="http"][class*="product-image"]'
        _field_pincode_='input[id="pincode"][placeholder="Enter Pincode"]'
        _field_check_availability_='button[onclick="deliverycheck()"][type="button"]'
        _field_instock_msg_ = 'p[class="availability in-stock"]>span'


        url = self.parser.get('store','url')
        self.get(url)
        self.driver.maximize_window()

        try:
            close_popup = self.driver.find_element_by_css_selector(_field_close_pop_up_)
            close_popup.click()
            self.log("pop-up closed")

        except Exception:
            self.log("no pop-up")
            pass

        products = None
        found = False
        while not found:
            self.log("Trying to find a product...")
            try:
                menu_list = self.driver.find_elements_by_css_selector(_field_menu_list_)
                menu = menu_list.pop(0)
                category_list = menu.find_elements_by_css_selector(_field_category_list_)
                container = []
                for eachCategory in category_list:
                    container.append(eachCategory.find_element_by_css_selector(_field_container_).get_attribute('href'))
                url_categories = "#"
                while "#" in url_categories:
                    url_categories = random.choice(container)

                self.get(url_categories)
                self.log("Gotpage " + url_categories)
                product_list = self.driver.find_elements_by_css_selector(_field_product_list_)
                products = []
                for eachP in product_list:
                    products.append(eachP)
                if products == []:
                    raise NoSuchElementException
                else:
                    found = True
                    self.log("Category found")
            except NoSuchElementException:
                self.log("Category not found. Trying next category...")
                pass

        finalproductslist = []
        for eachn in products:
            finalproductslist.append(eachn.get_attribute('href'))


        product_found = False
        while product_found is False:
            product_url = random.choice(finalproductslist)
            self.get(product_url)
            self.log("Product url is.. " + product_url)
            self.log("at Add-to-Cart page")
            try:
                instock_message = self.driver.find_element_by_css_selector(_field_instock_msg_)
                self.log("Instock message =" + instock_message.text)
                product_found = True
            except Exception:
                self.log("product out of stock")

    def lotus_testing_addtocart(self):
        _field_configurations_container_ = 'div[class="amconf-images-container"]'
        _field_configurations_ = 'div[id*="amconf"]'
        _field_addtocartbutton_ = 'button[id*="product-addtocart-button"]'
        _field_viewcart_ = 'button[title="View Cart"]'
        _field_proceed_to_checkout_= 'button[class*="btn-proceed-checkout"]'

        try:
            script = "window.scrollBy(0,70)"
            self.driver.execute_script(script)
            self.log("Scrolled Page")
            self.driver.implicitly_wait(5)
            configuration_container = self.driver.find_elements_by_css_selector(_field_configurations_container_)
            configlist = []
            for container in configuration_container:
                configurations = container.find_elements_by_css_selector(_field_configurations_)
                for config_el in configurations:
                    configlist.append(config_el)

            configurations = None
            while configurations is None:
                configurations = random.choice(configlist)
                configurations.click()
                self.log("we have a configurable product")
        except Exception:
            pass
            self.log("we have a non-configurable product ")

        addtocart = self.driver.find_element_by_css_selector(_field_addtocartbutton_)
        try:
            addtocart.click()
            self.log("buy now clicked")
        except NoSuchElementException:
            addtocart.submit()
            self.log("could not click buy now button")
            pass

        try:
            viewcart = self.driver.find_element_by_css_selector(_field_viewcart_)
            viewcart.click()
        except Exception:
            checkout_url = self.parser.get('url', 'checkout_url')
            self.driver.get(checkout_url)
            self.log("found cart page")
            pass
        self.log("current url =" + self.driver.current_url)
        time.sleep(2)
        proceed_to_checkout = self.driver.find_elements_by_css_selector(_field_proceed_to_checkout_)
        checkout = proceed_to_checkout.__getitem__(0)
        checkout.click()
        self.log("proceed-to-checkout button clicked")
        self.log("at checkout page")
        self.log("current url = "+ self.driver.current_url)
        self.driver.implicitly_wait(5)

    def lotus_testing_checkout(self):
        #locators

        _field_firstname_ = 'input[Placeholder*="First Name"][title="First Name"]'
        _field_lastname_ = 'input[Placeholder*="Last Name"][title="Last Name"]'
        _field_phone_number = 'input[type*="number"][placeholder*="10 digit number"]'
        _field_email_ = 'input[title*="Email Address"][placeholder*="Email Address"]'
        _field_address_ = 'input[type*="text"][placeholder ="Flat/House No. Colony/Street No."]'
        _field_city_='input[title = "City"][placeholder="City"]'
        _field_state_dropdown_= './/select[@title = "State/Province"]'


        _keys_email = 'testcase@shoptimize.in'
        _keys_firstname = 'test'
        _keys_lastname = 'case'
        _keys_street = '1/1 aundh/street'
        _keys_phonenumber = '1234567890'
        _keys_city ='Pune'

        time.sleep(5)

        script = "window.scrollBy(0, 20);"
        self.driver.execute_script(script)

        firstname = self.driver.find_elements_by_css_selector(_field_firstname_).pop(0)
        firstname.send_keys(_keys_firstname)
        self.log("entered First Name")

        lastname = self.driver.find_elements_by_css_selector(_field_lastname_).pop(0)
        lastname.send_keys(_keys_lastname)
        self.log("entered last Name")

        phone_number = self.driver.find_elements_by_css_selector(_field_phone_number).pop(0)
        phone_number.send_keys(_keys_phonenumber)
        self.log("entered Phone Number")

        email = self.driver.find_element_by_css_selector(_field_email_)
        email.send_keys(_keys_email)
        self.log("entered emailID")

        time.sleep(2)
        self.driver.execute_script(script)
        street_address = self.driver.find_element_by_css_selector(_field_address_)
        street_address.send_keys(_keys_street)
        self.log("entered Address")
        time.sleep(2)

        city = self.driver.find_element_by_css_selector(_field_city_)
        city.send_keys(_keys_city)
        self.log("entered city")

        state_dropdown = self.driver.find_element_by_xpath(_field_state_dropdown_)
        select_state = Select(state_dropdown)
        select_state.select_by_value("505")
        self.log("entered State")
        time.sleep(2)
        self.log(" Finished checkout Test")

        place_order = self.driver.find_element_by_css_selector('button[class*="btn-checkout"][title*="Place Order Now"]')
        self.log(place_order)
        if place_order.text > 0:
            pass
        else:
            raise ElementNotVisibleException






