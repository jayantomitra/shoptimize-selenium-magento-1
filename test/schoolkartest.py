from selenium.common.exceptions import NoSuchElementException
from loginPage import LoginPage
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.webdriver.support.ui import Select
import random
import time

class Schoolkarttest(LoginPage):
    def __init__(self, configfile, should_login=True):
        LoginPage.__init__(self, configfile)
        self.should_login = should_login

    def run_test(self):
        self.schoolkarttest()
        if self.should_login:
            super(Schoolkarttest, self).run_test()
            self.clearcart()
            self.schoolkarttest()


    def clearcart(self):
        self.log("clearcart method started")
        try:
            checkout_url = self.parser.get('url','checkout_url')
            self.get(checkout_url)
            clear_cart_button = self.driver.find_element_by_css_selector('button[class*="btn-empty"][id="empty_cart_button"]')
            clear_cart_button.click()
            self.log("cart cleared")
            self.log(self.driver.current_url)
        except Exception:
            pass

    def schoolkarttest(self):
        _field_close_pop_up_='button[type*="button"][data-dismiss ="modal"]'
        _field_menu_list_='ul[class*="nav nav-wide topmenu navbar-nav"]'
        _field_category_list_ = 'li[class*="level0"]'
        _field_container_='a[class="level-top"]'
        _field_product_list_='a[href*="http"][class*="product-image"]'
        _field_pincode_='input[id="pincode"][placeholder="Enter Pincode"]'
        _field_check_availability_='button[onclick="deliverycheck()"][type="button"]'
        _field_instock_msg_ = 'p[class="availability in-stock"]>span'
        _field_configurations_container_= 'div[class="amconf-images-container"]'
        _field_configurations_='div[id*="amconf"]'
        _field_addtocartbutton_='button[class ="button btn-cart"]'
        _field_viewcart_ = 'button[title= "View Cart"]'
        _field_proceed_to_checkout_= 'button[title*= "Proceed to Checkout"]'


        url = self.parser.get('store','url')
        self.get(url)
        self.driver.maximize_window()

        try:
            close_popup = self.driver.find_element_by_css_selector(_field_close_pop_up_)
            close_popup.click()
            self.log("pop-up closed")

        except Exception:
            self.log("no pop-up")
            pass

        products = None
        found = False
        while not found:
            self.log("Trying to find a product...")
            try:
                menu_list = self.driver.find_elements_by_css_selector(_field_menu_list_)
                menu = menu_list.pop(0)
                category_list = menu.find_elements_by_css_selector(_field_category_list_)
                container = []
                for eachCategory in category_list:
                    container.append(eachCategory.find_element_by_css_selector(_field_container_).get_attribute('href'))
                url_categories = "#"
                while "#" in url_categories:
                    url_categories = random.choice(container)

                self.get(url_categories)
                self.log("Gotpage " + url_categories)
                product_list = self.driver.find_elements_by_css_selector(_field_product_list_)
                products = []
                for eachP in product_list:
                    products.append(eachP)
                if len(products) == 0:
                    raise NoSuchElementException
                else:
                    found = True
                    self.log("Product found")
            except NoSuchElementException:
                self.log("Product not found. Trying next category...")
                pass

        finalproductslist = []
        for eachn in products:
            finalproductslist.append(eachn.get_attribute('href'))

        product_found = False
        while product_found is False:
            product_url = random.choice(finalproductslist)
            self.get(product_url)
            self.log("Product url is.. " + product_url)
            self.log("at Add-to-Cart page")

            try:
                instock_message = self.driver.find_element_by_css_selector(_field_instock_msg_)
                self.log("Instock message =" + instock_message.text)
                product_found = True
            except Exception:
                product_found = False
                self.log("product out of stock")
        try:
            script = "window.scrollBy(0,70)"
            self.driver.execute_script(script)
            self.log("Scrolled Page")
            self.driver.implicitly_wait(5)
            configuration_container = self.driver.find_elements_by_css_selector(_field_configurations_container_)
            configlist = []
            for container in configuration_container:
                configurations = container.find_elements_by_css_selector(_field_configurations_)
                for config_el in configurations:
                    configlist.append(config_el)

            configurations = None
            while configurations is None:
                configurations = random.choice(configlist)
                configurations.click()

                self.log("we have a configurable product")
        except Exception:
            pass
            self.log("we have a non-configurable product ")


        addtocart = self.driver.find_element_by_css_selector(_field_addtocartbutton_)
        try:
            addtocart.click()

            self.log("buy now clicked")
        except NoSuchElementException:
            addtocart.submit()

            self.log("could not click buy now button")
            pass

        try:
            viewcart = self.driver.find_element_by_css_selector(_field_viewcart_)
            viewcart.click()
        except Exception:
            checkout_url = self.parser.get('url', 'checkout_url')
            self.driver.get(checkout_url)
            self.log("found cart page")
            pass
        self.log("current url =" + self.driver.current_url)
        time.sleep(2)
        proceed_to_checkout = self.driver.find_elements_by_css_selector(_field_proceed_to_checkout_)
        checkout = proceed_to_checkout.__getitem__(0)

        checkout.click()
        self.log("proceed-to-checkout button clicked")

        self.log("at checkout page")
        self.log("current url = "+ self.driver.current_url)

        place_order = self.driver.find_element_by_css_selector('button[class*="btn-checkout"][title*="Place Order Now"]')
        self.log(place_order)
        if place_order.text > 0:
            pass
        else:
            raise ElementNotVisibleException







