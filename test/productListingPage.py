from test.loginPage import LoginPage 
import time
import random
from selenium.common.exceptions import NoSuchElementException


class ProductListing(LoginPage):
    def __init__(self, configfile, should_login=True):
        LoginPage.__init__(self, configfile)
        self.should_login = should_login

    def run_test(self):
        if self.should_login:
            super(ProductListing, self).run_test()
        self.login()

    def productlistingpage(self):

          # locators
        _field_productlist_ = '[id*="vesitem"]'
        _field_listofproducts_navanchor_ = '[class*=" nav-anchor"]'
        _field_product_photo_ = 'a[href*="http"][class*="product photo"]'

        self.log("started running productlistingpage test")

        url = self.parser.get('store','url')
        print "getting URL" + url
        response_time = self.get(url)
        print "got URL" + url
        self.log(response_time)
        self.driver.maximize_window()
        time.sleep(5)
        product = None
        found = False
        while not found:
            print "Trying to find a product..."
            try:
                productlist = self.driver.find_elements_by_css_selector(_field_productlist_)

                list_of_products = []
                for eachP in productlist:
                    list_of_products.append(eachP.find_element_by_css_selector(_field_listofproducts_navanchor_).get_attribute('href'))

                url_products = "#"
                while "#" in url_products:
                    url_products = random.choice(list_of_products)

                print("Getting..." + url_products)
                response_time = self.get(url_products)
                self.log(response_time)
                print("Got page " + url_products)
                product = self.driver.find_elements_by_css_selector(_field_product_photo_)

                if len(product) == 0:
                    raise NoSuchElementException
                else:
                    found = True
                    self.log("Product found")
            except NoSuchElementException:
                self.log("Product not found. Trying next category...")
                pass

        finallist = []
        for eachn in product:
            finallist.append(eachn.get_attribute('href'))
        finalurl = False
        while not finalurl:
            print "again"
            furl = random.choice(finallist)
            response_time = self.get(furl)
            self.log(response_time)
            self.log("Product url is.. " + furl)
            self.driver.implicitly_wait(5)
            try:
                instock_message = self.driver.find_element_by_css_selector('div[class="stock available"][title ="Availability"]')
                if len(instock_message.text) == 0:
                    print "out of stock"
                    raise NoSuchElementException
                else:
                    finalurl = True
                    print "got instock product exiting"
            except NoSuchElementException:
                pass