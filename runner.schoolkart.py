import sys
import unittest
from utils.emailer import Emailer
from test.schoolkartest import Schoolkarttest
from test.homepage import HomePage


class Runner(unittest.TestCase):
    config = ""

    switch = {}

    store_title = ""

    emailer = None

    @staticmethod
    def set_config(config):
        Runner.config = config
        Runner._load_runner_switches()

    @staticmethod
    def _load_runner_switches():
        ''' loads runner switches from configuration file '''
        sk =Schoolkarttest(Runner.config)
        Runner.store_title = sk.parser.get('store', 'code')
        Runner.emailer = Emailer(store_title=Runner.store_title)
        for option in sk.parser.options("runner"):
            Runner.switch[option] = sk.parser.getboolean("runner", option)

    def _get_formatted_message(self, msg_list):
        ''' formats message list and adds <br> at the end of each message '''
        final_msg = ""
        for msg in msg_list:
            final_msg = final_msg + msg + "<br>"
        return final_msg

    def test_schoolkart(self):
         tsk = Schoolkarttest(Runner.config)
         Runner.store_title = tsk.parser.get('store', 'code')
         Runner.emailer = Emailer(store_title=Runner.store_title)
         try:
             tsk.run()
         except Exception, e:
             tsk.log("Internal error occurred " + str(e))
             Runner.emailer.add_new_message(tsk.log_messages)
             Runner.emailer.add_new_message(str(e))


    def test_send(self):
        ''' calls emailer's send method '''
        Runner.emailer.send("AKIAJR6CGBATWLN6XJYQ", "AlV4qEhgFEMkQcr+zL6hV0ujieKSfaf7prmubv92CPGz")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Config file not provided. Exiting..."
        sys.exit(1)
    Runner.set_config(sys.argv.pop())
    unittest.main()