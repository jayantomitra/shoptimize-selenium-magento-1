import os
import sys
import smtplib
from timers import Time
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class Emailer:
    ''' SMTP email healper '''

    # wait for this many seconds before sending next failure email
    EMAIL_COOLDOWN_PERIOD = 90

    # stores timestamp of when last email was sent in this file
    TEMP_STAMP_EMAIL = os.path.dirname(os.path.abspath(__file__)) + "/magento-1/utils/tmp/.__store_code__.email.stamp"

    DEFAULT_SMTP_SERVER  = "email-smtp.us-east-1.amazonaws.com"
    DEFAULT_SMTP_PORT    = "587"
    DEFAULT_FROM_ADDRESS = "devops@shoptimize.in"

    # Healthcheck system keeps adding different messages to this global array. at the
    # end of execution all messages in the list are appended together and sent in email
    MESSAGES = []

    # list of recipients
    RECIPIENTS = ["jayanto@shoptimize.in", "ravi@shoptimize.in","tushar@shoptimize.in"]

    # placeholder message for email
    DEFAULT_MESSAGE = """
        <html>
        <head>
            <title>Shoptimize Automation Tests</title>
        </head>
        <body>
            <p><b>Store Code : </b>__store_title__</p>
            <p>
                __msg__
                <br><br>
            </p>
        </body>
        </html>
    """

    def __init__(self, server=None, port=None, from_add=None, store_title=None):
        ''' '''
        self._load_defaults(server, port, from_add)
        self.store_title = store_title

    def _load_defaults(self, server, port, from_add):
        ''' loads default values '''
        if server is None:
            server = Emailer.DEFAULT_SMTP_SERVER
        if port is None:
            port = Emailer.DEFAULT_SMTP_PORT
        if from_add is None:
            from_add = Emailer.DEFAULT_FROM_ADDRESS
        self.server   = server
        self.port     = port
        self.from_add = from_add


    def _get_temp_tstamp_file(self):
        ''' returns name of the tstamp file for given store '''
        return Emailer.TEMP_STAMP_EMAIL.replace('__store_code__', self.store_title)

    def _get_final_messages(self):
        ''' combines all messages in the list together and replaces email message placeholders '''
        final_msgs = "<br><br> ".join(Emailer.MESSAGES)
        return final_msgs

    def _generate_healthcheck_final_emails(self):
        ''' generates final email to be sent '''
        final_msgs = self._get_final_messages()

        # prepare email message by replacing placeholders with real values
        msg_to_send = Emailer.DEFAULT_MESSAGE
        msg_to_send = msg_to_send.replace('__msg__', final_msgs)
        msg_to_send = msg_to_send.replace('__store_title__', self.store_title)
        msg_to_send = MIMEText(msg_to_send, 'html')

        recipients = ', '.join(Emailer.RECIPIENTS)

        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Shoptimize Automation Tests Failure - " + self.store_title
        msg['From'] = self.from_add
        msg['To'] = recipients
        msg.attach(msg_to_send)

        return msg

    def _get_last_email_sent_tstamp(self):
        ''' checks when was the last incident created by Healthcheck System for this store '''
        tstamp = 0
        try:
            f = open(self._get_temp_tstamp_file(), 'r')
            tstamp = f.read()
            f.close()
        except:
            pass
        return tstamp

    def _now_time_stamp(self):
        ''' returns current timestamp as a string '''
        return str(Time.dt2ts(Time.get_current_time(tzone='UTC')))

    def _update_incident_tstamp(self):
        ''' updates incident creation timestamp '''
        f = open(self._get_temp_tstamp_file(), 'w+')
        tstamp = self._now_time_stamp()
        f.write(tstamp)
        f.close()

    def _should_send(self):
        ''' decides whether e-mail is to be sent or not based on DEFAULT_EMAIL_COOLDOWN period '''
        last_stamp = Time.ts2dt(self._get_last_email_sent_tstamp())
        now = Time.ts2dt(self._now_time_stamp())
        if Time.get_difference(now, last_stamp) > Emailer.EMAIL_COOLDOWN_PERIOD:
            return True         # this is when elasta alert runs
        return False

    @classmethod
    def add_new_message(self, msg, special=False):
        ''' adds new message to list of healthcheck messages '''
        if type(msg) is list:
            Emailer.MESSAGES.extend(msg)
        else:
            Emailer.MESSAGES.append(msg)

    @classmethod
    def add_new_recipient(self, msg, special=False):
        ''' adds new message to list of healthcheck messages '''
        Emailer.RECIPIENTS.append(msg)

    def send(self, user, password):
        ''' sends email '''
        # check whether there are any messages to send
        server = smtplib.SMTP(self.server, self.port)
        server.starttls()
        server.login(user, password)
        if self._should_send():
            mail = self._generate_healthcheck_final_emails()
            if Emailer.MESSAGES:
                server.sendmail(self.from_add, Emailer.RECIPIENTS, mail.as_string())
            self._update_incident_tstamp()
        else:
            print "Emailer waiting for cool down period to finish..."
        server.quit()
