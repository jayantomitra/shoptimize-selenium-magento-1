import sys
import unittest
from utils.emailer import Emailer
from test.luminoustest import Luminoustest
from test.homepage import HomePage

class Runner(unittest.TestCase):
    config = ""

    switch = {}

    store_title = ""

    emailer = None

    @staticmethod
    def set_config(config):
        Runner.config = config
        Runner._load_runner_switches()

    @staticmethod
    def _load_runner_switches():
        ''' loads runner switches from configuration file '''
        hp = HomePage(Runner.config)
        Runner.store_title = hp.parser.get('store', 'code')
        Runner.emailer = Emailer(store_title=Runner.store_title)
        for option in hp.parser.options("runner"):
            Runner.switch[option] = hp.parser.getboolean("runner", option)

    def test_luminous(self):
         lm = Luminoustest(Runner.config)
         try:
             lm.run()
         except Exception, e:
             lm.log("Internal error occurred " + str(e))
             Runner.emailer.add_new_message(lm.log_messages)
             Runner.emailer.add_new_message(str(e))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Config file not provided. Exiting..."
        sys.exit(1)
    Runner.set_config(sys.argv.pop())
    unittest.main(verbosity=2)