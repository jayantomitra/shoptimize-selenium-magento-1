from addToCartPage import AddToCart
import time
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException


class CheckoutTest(AddToCart):

    def __init__(self, configfile):
        AddToCart.__init__(self, configfile)


    def run_test(self, login=True):
        self.log("started running checkout test")
        super(CheckoutTest, self).run_test()
        self.driver.implicitly_wait(10)

           # locators
        _proceed_button_ = 'button[class*="action primary checkout"]'
        _field_email_ = 'input[class*="input-text"][id*="customer-email"]'
        _field_firstname_ = 'input[class="input-text"][name="firstname"]'
        _field_lastname_ = 'input[class="input-text"][name*="lastname"]'
        _field_street_address_ = 'input[class*="input-text"][name*="street[0]"]'
        _field_city_ = 'input[name="city"]'
        _field_state_dropdown_ = '//select[@name="region_id"]'
        _field_postal_address = '//input[@name="postcode"]'
        _field_country_dropdown = '//select[@name="country_id"]'
        _field_phone_number = 'input[name="telephone"]'
        _field_next_ = 'button[class*= "button action continue primary"]'

           # Data send keys
        _keys_email = 'testcase@shoptimize.in'
        _keys_firstname = 'test'
        _keys_lastname = 'case'
        _keys_street = 'street'
        _keys_city = 'Pune'
        _keys_postaladdress = '411007'
        _keys_phonenumber = '1234567890'

        proceed = self.driver.find_element_by_css_selector(_proceed_button_)
        proceed.click()
        time.sleep(5)
        script = "window.scrollBy(0, 20);"
        email = self.driver.find_element_by_css_selector(_field_email_)
        email.send_keys(_keys_email)
        time.sleep(2)
        self.driver.execute_script(script)
        firstname = self.driver.find_element_by_css_selector(_field_firstname_)
        firstname.send_keys(_keys_firstname)
        lastname = self.driver.find_element_by_css_selector(_field_lastname_)
        lastname.send_keys(_keys_lastname)
        self.driver.execute_script(script)
        street_address = self.driver.find_element_by_css_selector(_field_street_address_)
        street_address.send_keys(_keys_street)
        city = self.driver.find_element_by_css_selector(_field_city_)
        city.send_keys(_keys_city)
        time.sleep(2)
        state_dropdown = self.driver.find_element_by_xpath(_field_state_dropdown_)
        select_state = Select(state_dropdown)
        try :
            select_state.select_by_value("505")
            self.driver.implicitly_wait(5)
        except NoSuchElementException:
            select_state.select_by_value("532")
            self.driver.implicitly_wait(5)
            pass
        self.driver.execute_script(script)
        postal_address = self.driver.find_element_by_xpath(_field_postal_address)
        postal_address.send_keys(_keys_postaladdress)
        time.sleep(2)
        country_dropdown = self.driver.find_element_by_xpath(_field_country_dropdown)
        select_country = Select(country_dropdown)
        select_country.select_by_value('IN')
        time.sleep(2)
        phone_number = self.driver.find_element_by_css_selector(_field_phone_number)
        phone_number.send_keys(_keys_phonenumber)
        time.sleep(10)

