import sys
import unittest
from utils.emailer import Emailer
from test.loginPage import LoginPage
from test.productListingPage import ProductListing
from test.addToCartPage import AddToCart
from test.checkouttest import CheckoutTest
from test.homepage import HomePage
from test.haldiramtest import Haldiramtest
from test.schoolkartest import Schoolkarttest
from test.luminoustest import Luminoustest

class Runner(unittest.TestCase):
    config = ""
    
    switch = {}

    store_title = ""

    emailer = None

    @staticmethod
    def set_config(config):
        Runner.config = config
        Runner._load_runner_switches()
    
    @staticmethod
    def _load_runner_switches():
        ''' loads runner switches from configuration file '''
        hp = HomePage(Runner.config)
        Runner.store_title = hp.parser.get('store', 'code')
        Runner.emailer = Emailer(store_title=Runner.store_title)
        for option in hp.parser.options("runner"):
            Runner.switch[option] = hp.parser.getboolean("runner", option)


    def _get_formatted_message(self, msg_list):
        ''' formats message list and adds <br> at the end of each message '''
        final_msg = ""
        for msg in msg_list:
            final_msg = final_msg + msg + "<br>"
        return final_msg

    def test_homepage(self):
        hp = HomePage(Runner.config)
        if Runner.switch["homepage"]:
            try:
                hp.run()
            except Exception, e:
                hp.log("Internal error occurred at homepage test" + str(e))
                Runner.emailer.add_new_message(hp.log_messages)
                Runner.emailer.add_new_message(str(e))

    def test_login(self):
        lp = LoginPage(Runner.config)
        if Runner.switch["login"]:        
            try:
                lp.run()
            except Exception, e:
                lp.log("Internal error occurred login test" + str(e))
                Runner.emailer.add_new_message(lp.log_messages)
                Runner.emailer.add_new_message(str(e))


    def test_productlistingpage(self):
        plp = ProductListing(Runner.config, should_login=True)
        if Runner.switch["productlisting"]:
            try:
                plp.run()
            except Exception, e:
                plp.log("Internal error occurred product listing page test" + str(e))
                Runner.emailer.add_new_message(plp.log_messages)
                Runner.emailer.add_new_message(str(e))


    def test_addtocart(self):
        atc = AddToCart(Runner.config)
        if Runner.switch["addtocart"]:        
            try:
                atc.run()
            except Exception, e:
                atc.log("Internal error occurred add to cart test" + str(e))
                Runner.emailer.add_new_message(atc.log_messages)
                Runner.emailer.add_new_message(str(e))

    def test_checkout(self):
        cot = CheckoutTest(Runner.config)
        if Runner.switch["checkout"]:        
            try:
                cot.run()
            except Exception, e:
                cot.log("Internal error occurred checkout test" + str(e))
                Runner.emailer.add_new_message(cot.log_messages)
                Runner.emailer.add_new_message(str(e))

    # def test_haldiram(self):
    #     thm = Haldiramtest(Runner.config)
    #     try:
    #         thm.run()
    #     except Exception, e:
    #         thm.log("Internal error occurred " + str(e))
    #         Runner.emailer.add_new_message(thm.log_messages)
    #         Runner.emailer.add_new_message(str(e))

    # def test_schoolkart(self):
    #      tsk = Schoolkarttest(Runner.config)
    #      try:
    #          tsk.run()
    #      except Exception, e:
    #          tsk.log("Internal error occurred " + str(e))
    #          Runner.emailer.add_new_message(tsk.log_messages)
    #          Runner.emailer.add_new_message(str(e))

    # def test_luminous(self):
    #     tlm = Luminoustest(Runner.config)
    #     try:
    #         tlm.run()
    #     except Exception, e:
    #         tlm.log("Internal error occurred " + str(e))
    #         Runner.emailer.add_new_message(tlm.log_messages)
    #         Runner.emailer.add_new_message(str(e))


    def test_send(self):
        ''' calls emailer's send method '''
        Runner.emailer.send("AKIAJR6CGBATWLN6XJYQ", "AlV4qEhgFEMkQcr+zL6hV0ujieKSfaf7prmubv92CPGz")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Config file not provided. Exiting..."
        sys.exit(1)
    Runner.set_config(sys.argv.pop())
    unittest.main()
