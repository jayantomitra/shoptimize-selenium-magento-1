import time
from baseclass import BaseClass


class LoginPage(BaseClass):

    def __init__(self, configfile):
        BaseClass.__init__(self, configfile)

    def run_test(self):
        super(LoginPage, self).run_test()
        login_url = self.parser.get('url', 'login_url')
        res_time = self.get(login_url)
        self.log(res_time)
        self.driver.maximize_window()
        time.sleep(10)
        self.username = self.parser.get('user', 'username')
        self.password = self.parser.get('user', 'password')
        self.login()

    def login(self):
        self.log("started running login test")
        # locators
        _field_email = 'input[id="email"]'
        _field_password = 'input[id="pass"][title*="Password"]'
        _field_loginbutton = 'button[class*="button"][title*="Login"]'
        email = self.driver.find_element_by_css_selector(_field_email)
        email.send_keys(self.username)
        pass_word = self.driver.find_element_by_css_selector(_field_password)
        pass_word.send_keys(self.password)
        login_button = self.driver.find_element_by_css_selector(_field_loginbutton)
        login_button.submit()
        time.sleep(5)
        self.log("user is now logged in")   


