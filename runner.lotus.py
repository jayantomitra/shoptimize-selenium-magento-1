import sys
import unittest
from utils.emailer import Emailer
from test.lotustest import Lotustest
from test.homepage import HomePage


class Runner(unittest.TestCase):
    config = ""

    switch = {}

    store_title = ""

    emailer = None

    @staticmethod
    def set_config(config):
        Runner.config = config
        Runner._load_runner_switches()

    @staticmethod
    def _load_runner_switches():
        ''' loads runner switches from configuration file '''
        hp = HomePage(Runner.config)
        Runner.store_title = hp.parser.get('store', 'code')
        Runner.emailer = Emailer(store_title=Runner.store_title)
        for option in hp.parser.options("runner"):
            Runner.switch[option] = hp.parser.getboolean("runner", option)

    def _get_formatted_message(self, msg_list):
        ''' formats message list and adds <br> at the end of each message '''
        final_msg = ""
        for msg in msg_list:
            final_msg = final_msg + msg + "<br>"
        return final_msg

    def test_Lotus(self):
         lt = Lotustest(Runner.config)
         try:
             lt.run()
         except Exception, e:
             lt.log("Internal error occurred " + str(e))
             Runner.emailer.add_new_message(lt.log_messages)
             Runner.emailer.add_new_message(str(e))

    def test_send(self):
        ''' calls emailer's send method '''
        Runner.emailer.send("AKIAJR6CGBATWLN6XJYQ", "AlV4qEhgFEMkQcr+zL6hV0ujieKSfaf7prmubv92CPGz")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Config file not provided. Exiting..."
        sys.exit(1)
    Runner.set_config(sys.argv.pop())
    unittest.main()