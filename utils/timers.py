import os
import sys
from pytz import timezone
from datetime import datetime
from datetime import timedelta

class Time():
    ''' time utilities '''
    DEFAULT_TIMEZONE = 'Asia/Kolkata'

    @classmethod
    def get_current_time(self, tzone=None, humanize=False):
        ''' returns current time ''' 
        ''' returns time as human readable string if readable is set '''
        ''' returns time as datetime object instead '''
        if tzone is None:
            tzone = Time.DEFAULT_TIMEZONE
        tzone = timezone(tzone)
        tm = datetime.now(tzone)
        if humanize:
            return tm.strftime('%a %b %d,%Y %H:%M:%S %Z%z')
        return tm

    @classmethod
    def get_future_time(self, tzone=None, seconds=None, format=''):
        ''' returns future time by adding specified number of seconds in current time '''
        return self.get_current_time(tzone=tzone) + datetime.timedelta(seconds=seconds) 

    @classmethod
    def get_past_time(self, tzone=None, seconds=None):
        ''' returns time before past_seconds number of seconds '''
        return self.get_current_time(tzone=tzone) - timedelta(seconds=seconds)

    @classmethod
    def ts2dt(self, timestamp=None):
        ''' returns datetime object from Unix timestamp string '''
        timestamp = int(timestamp)
        dt = datetime.fromtimestamp(timestamp)
        return dt

    @classmethod
    def schedule_to_date(self, schedule, tzone=None):
        ''' converts schedule to date objects '''
        ''' schedules in format HH:MM-HH:MM '''
        schedules = schedule 
        min_time  = schedules.split('-')[0]
        max_time  = schedules.split('-')[1]
        min_hours   = int(min_time.split(':')[0])
        min_minutes = int(min_time.split(':')[1])
        max_hours   = int(max_time.split(':')[0])
        max_minutes = int(max_time.split(':')[1])

        if tzone is None:
            tzone = Time.DEFAULT_TIMEZONE
        tzone = timezone(tzone)

        now = datetime.now(tzone)

        min_limit = tzone.localize(datetime(now.year, now.month, now.day, min_hours, min_minutes, 0))
        max_limit = tzone.localize(datetime(now.year, now.month, now.day, max_hours, max_minutes, 0))

        return now, min_limit, max_limit

    @classmethod
    def get_difference(self, dt1, dt2):
        ''' returns difference in seconds between two datetime objects '''
        dt1 = dt1.replace(tzinfo=None)
        dt2 = dt2.replace(tzinfo=None)
        return int((dt1 - dt2).total_seconds())

    @classmethod
    def dt2ts(self, dt):
        ''' converts datetime to unix timezone '''
        import calendar
        import time
        from dateutil import tz
    
        if dt.tzinfo is None:
            return int(time.mktime(dt.timetuple()))
        utc_dt = dt.astimezone(tz.tzutc()).timetuple()
        return calendar.timegm(utc_dt)