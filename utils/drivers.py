from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class ChromeDriver:

    capabilities = None

    def __init__(self, enable_log=True):
        d = DesiredCapabilities.CHROME
        if enable_log:
            d['loggingPrefs'] = {'performance': 'ALL'}
        self.capabilities = d

    def setupHeadless(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        return webdriver.Chrome(options=options)


    def setupRegular(self):
        options = webdriver.ChromeOptions()
        return webdriver.Chrome(options=options,
                                service_args=["--verbose", "--log-path=/tmp/chromedriver1.log"],
                                desired_capabilities=self.capabilities)


    def setupRemote(self):
        return webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME)
