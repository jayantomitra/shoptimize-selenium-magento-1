import time
from baseclass import BaseClass


class HomePage(BaseClass):
    def __init__(self, configfile):
        BaseClass.__init__(self,configfile)

    def run_test(self):
        super(HomePage, self).run_test()
        self.homepage()

    def homepage(self):
        url = self.parser.get('store','url')
        response_time = self.get(url)
        self.log(response_time)
        self.log("started running homepage test")
        self.driver.maximize_window()
        time.sleep(5)
        footnote = self.driver.find_element_by_xpath("//a[@href='http://www.shoptimize.in/']")
        try:
            self.assertIsNotNone(footnote, "link text not found")
            footnoter = str(footnote.text).lower()
            self.assertEqual(footnoter, 'powered by shoptimize')
        except Exception:
            pass
        link = footnote.get_attribute('href')
        self.assertEqual(link, 'http://www.shoptimize.in/')
        time.sleep(5)


