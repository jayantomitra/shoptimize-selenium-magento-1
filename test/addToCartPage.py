import time
import random
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from productListingPage import ProductListing


class AddToCart(ProductListing):

    def __init__(self, configfile):
        ProductListing.__init__(self, configfile)

    def run_test(self, login=False):
        super(AddToCart, self).run_test()
        self.addtocarttest()

    def addtocarttest(self):
        # locators
        _field_containers_ = 'div[class*="swatch-attribute-options"]'
        _field_configs_ = 'div[class*="swatch-option"]'
        _field_addtocartbutton_ = 'button[title*="Add to Cart"]'
        _field_to_cart_ = '//a[@class="action showcart"]'
        _field_empty_cart_ = '//div[@class="cart-empty"]'
        time.sleep(5)


        print "trying"
        try:
            configurations_container = self.driver.find_elements_by_css_selector(_field_containers_)
            for container in configurations_container:
                configs = container.find_elements_by_css_selector(_field_configs_)

            configlist = []
            for config_el in configs:
                configlist.append(config_el)

            configurations = None

            while configurations is None:
                configurations = random.choice(configlist)
                configurations.click()

            print("we have a configurable product")
        except Exception:
            pass
            print("we have a non-configurable product ")
            self.driver.implicitly_wait(5)

        try:
            addtocartbutton = self.driver.find_element_by_css_selector(_field_addtocartbutton_)
            addtocartbutton.click()
            print "clicked on add to cart"
        except ElementNotVisibleException:
            time.sleep(5)
            try:
                self.driver.implicitly_wait(10)
                script = "window.scrollBy(0, 25);"
                self.driver.execute_script(script)
                addtocart_button = self.driver.find_element_by_css_selector('button[title*="Add to Cart"][type="submit"]')
                print "clicked on atc"
                addtocart_button.submit()
            except ElementNotVisibleException:
                self.log("addtocartbutton not visible")
                pass
            else:
                pass
        try:
            tocart = self.driver.find_element_by_xpath(_field_to_cart_)
            url_cart = tocart.get_attribute('href')
            response_time = self.get(url_cart)
            self.log(response_time)
            tocart.click()
            self.driver.implicitly_wait(5)
        except Exception:
            print "on cart page"
            pass
        try:
            empty_cart = self.driver.find_element_by_xpath(_field_empty_cart_)
            self.assertIsNotNone(empty_cart, "cart empty & add to cart test fails")
        except NoSuchElementException:
            print('product is in the cart')
