import inspect
import logging

from datetime import datetime

#import tzlocal # $ pip install tzlocal

#def posix2local(timestamp, tz=tzlocal.get_localzone()):
   # """Seconds since the epoch -> local time as an aware datetime object."""
    #return datetime.fromtimestamp(timestamp, tz)

#class CustomFormatter(logging.Formatter):
 #   def converter(self, timestamp):
  #      return posix2local(timestamp)

    # def formatTime(self, record, datefmt=None):
    #     dt = self.converter(record.created)
    #     if datefmt:
    #         s = dt.strftime(datefmt)
    #     else:
    #         t = dt.strftime(self.default_time_format)
    #         s = self.default_msec_format % (t, record.msecs)
    #     return s

def customlogger(loglevel=logging.DEBUG):
    loggerName = inspect.stack()[1][3]
    logger = logging.getLogger(loggerName)
    logger.setLevel(logging.DEBUG)
    fileHandler = logging.FileHandler("automation.log", mode='w+')
    fileHandler.setLevel(loglevel)
    #formatter = CustomFormatter('[%(levelname)s] [%(asctime)s] %(message)s', datefmt='%d/%b/%Y:%H:%M:%S %z')
    formatter = logging.Formatter('[%(levelname)s] [%(asctime)s] %(message)s', datefmt='%d/%b/%Y:%H:%M:%S %z')
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

    return logger

